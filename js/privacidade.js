window.addEventListener('load', function () {

    var accepted = localStorage.getItem('lgpd-accepted');
    if (accepted) {
      console.log('LGPD jÃ¡ foi aceito');
      return;
    }
  
    var box = document.getElementById('box-lgpd');
    if (!box) {
      console.log('Elemento com ID "box-lgpd" nÃ£o encontrado');
      // return;
    }
  
    var element = document.createElement('div');
    element.setAttribute('id', 'box-lgpd');
    element.innerHTML = 'Para proteger os seus dados, este site trata os dados pessoais de acordo com a Lei Geral de Proteção de Dados - LGPD.';
    
    var style = {
      position: 'fixed',
      left: '15px',
      bottom: '15px',
      padding: '30px',
      color: '#fff',
      backgroundColor: 'rgba(0,0,0,0.9)',
      borderRadius: '6px',
      width: '300px',
      fontSize: '12px',
      lineHeight: '18px',
    };
  
    for (var i in style) {
      element.style[i] = style[i];
    }
  
    var link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', 'https://privacidade.docszap.com/');
    link.innerHTML = 'Clique aqui para acessar nossa política de proteção de dados.';
  
    var linkStyle = {
      display: 'block',
      color: 'rgba(255,255,255,0.9)',
      textDecoration: 'underline',
    };
    
    for (var i in linkStyle) {
      link.style[i] = linkStyle[i];
    }
  
    element.appendChild(link);
  
    var button = document.createElement('button');
    button.setAttribute('type', 'button');
    button.innerHTML = 'Fechar';
    button.addEventListener('click', function () {
      localStorage.setItem('lgpd-accepted', 'yes');
      element.style.display = 'none';
    });
  
    var buttonStyle = {
      display: 'inline-block',
      color: '#fff',
      backgroundColor: '#3F72FE',
      border: 'none',
      borderRadius: '4px',
      padding: '6px 30px',
      marginTop: '20px',
    };
    
    for (var i in buttonStyle) {
      button.style[i] = buttonStyle[i];
    }
  
    element.appendChild(button);
  
    document.body.appendChild(element);
  });